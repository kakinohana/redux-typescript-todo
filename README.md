# todo app

- react
- redux
- react-redux
- styled-components
- typescript


```
yarn install
yarn start
```

## ディレクトリ構成
- Ducks

Redux Wayで作成したのものはこちら
- https://bitbucket.org/kakinohana/react-todo/src/master/


## 備考

以下にredux, react-redux, styled-componentsを入れて作成しました
- https://github.com/Microsoft/TypeScript-React-Starter

勉強する時に困った点などはこちらにまとめます
- https://willgate.atlassian.net/wiki/spaces/~kakinohana.jinai/pages/456622934/React+Redux+Typescript