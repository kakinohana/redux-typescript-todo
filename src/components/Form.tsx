import * as React from 'react'
import styled from 'styled-components'
import IFormProps, {IFormState} from '../types/Form'

/**
 * Component
 */
class Form extends React.Component<IFormProps, IFormState> {
  constructor (props: IFormProps) {
    super(props)
    this.state = {
      value: '',
      inputWidth: 200,
      buttonWidth: 50,
      placeholderColor: '#8f8f8f'
    }
    this.handleChange = this.handleChange.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
  }

  /**
   * render
   */
  public render () {
    return (
      <div>
        <form onSubmit={this.handleSubmit}>
          <Input
            type='text'
            placeholder={'タスク内容'}
            value={this.state.value}
            onChange={this.handleChange}
            // styled
            width={this.state.inputWidth}
            placeholderColor={this.state.placeholderColor} />
          <Button type='submit' width={this.state.buttonWidth} >追加</Button>
        </form>
      </div>
    )
  }

  /**
   * handleSubmit
   */
  private handleSubmit (e: React.FormEvent<HTMLFormElement>) {
    e.preventDefault()
    if (this.state.value === '') {
      return
    }
    this.setState({value: ''})

    return this.props.addTodo(this.state.value)
  }

  /**
   * handleChange
   */
  private handleChange (e: React.FormEvent<HTMLInputElement>) {
    this.setState({value: (e.target as HTMLInputElement).value})
  }
}

export default Form

const baseFormParts = `
appearance: none;
  height: 30px;
  border: 1px solid black;
  padding: 0 10px;
  box-sizing: border-box;
  vertical-align: bottom;
  font-size: 13px;
`

const Input = styled.input<{
  width: IFormState['inputWidth'];
  placeholderColor: IFormState['placeholderColor'];
}>`
  ${baseFormParts}
  width: ${({ width }) => width}px;
  color: ${({ placeholderColor }) => placeholderColor};
`
const Button = styled.button<{
  width: IFormState['buttonWidth'];
}>`
  ${baseFormParts}
  width: ${({ width }) => width}px;
`
