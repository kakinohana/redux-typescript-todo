import * as React from 'react'
import styled from 'styled-components'

interface IProps {
    text: string
}

export const Haeder = ({ text }: IProps) => {
  return (
    <Title>
      <header>
        {text}
      </header>
    </Title>
  )
}
export default Haeder

const Title = styled.h1`
  font-size: 18px;
  margin: 30px 0;
`
