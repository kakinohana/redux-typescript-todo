import * as React from 'react'
import styled, {css} from 'styled-components'
import ITodoEntityProps from '../types/Todo'
import {ITodoListEntityComponentsProps} from '../types/TodoList'


/**
 * Component
 */
class TodoList extends React.Component<ITodoListEntityComponentsProps> {
  constructor (props: ITodoListEntityComponentsProps) {
    super(props)
  }

  /**
   * render
   */
  public render () {
    const {todos} = this.props
    
    return (
      <TodoUl>
        {
          todos.map((todo: ITodoEntityProps) => {
            return (
              <TodoLi
                key={todo.id}
                onClick={this.handleClick.bind(this, todo)}
                completed={todo.completed} >

                {todo.name}
              </TodoLi>
            )
          })
        }
      </TodoUl>
    )
  }

  /**
   * handleClick
   */
  private handleClick = (todo: ITodoEntityProps, e: HTMLFormElement) => {
    return this.props.toggleTodo(todo)
  }
}

export default TodoList

const TodoUl = styled.ul`
  padding: 0;
  margin: 0;
  padding-left: 1em;
  margin-bottom: 20px;
`

const TodoLi = styled.li <{
  completed: ITodoEntityProps['completed']
}>`
  margin-bottom: 5px;
  cursor: pointer;

  ${({ completed }) =>
  completed &&
  css`
    text-decoration: line-through
  `
}
`