import actionCreatorFactory from 'typescript-fsa';
import ITodoEntityProps from '../types/Todo'

const actionCreator = actionCreatorFactory()

const InitialState: any[] = [{id: 1, name: 'init todo date', completed: false}]

enum TodoActionTypes {
  TOGGLE_TODO = 'TOGGLE_TODO',
  ADD_TODO = 'ADD_TODO'
}

// action creator
export const actions = {
  toggleTodo: actionCreator<ITodoEntityProps>(TodoActionTypes.TOGGLE_TODO),
  addTodo: actionCreator<{name: ITodoEntityProps['name']}>(TodoActionTypes.ADD_TODO)
}

// reducer
const todos = (state = InitialState, action: any) => {
  switch (action.type) {
    case TodoActionTypes.TOGGLE_TODO:
        return (
          state.map((todo) =>
            todo.id === action.payload.id ? {...todo, completed: !action.payload.completed} : todo
          )
        )
    case TodoActionTypes.ADD_TODO:
        return (
          [...state, {id: state.length+1, name: action.payload.name, completed: false}]
        )
    default:
      return state
  }
}

export default todos