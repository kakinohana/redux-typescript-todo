import TodoList from '@components/TodoList'
import { actions } from '@modules/TodoList'
import { connect } from 'react-redux'
import ITodoEntityProps from '../types/Todo';
import ITodoListEntityProps from '../types/TodoList'


const mapStateToProps = (state: ITodoListEntityProps) => {
  return { todos: state.todos }
}

const mapDispatchToProps = (dispatch: any) => ({
  toggleTodo: (todo: ITodoEntityProps) => {
    dispatch(actions.toggleTodo(todo))
  }
})

export default connect(mapStateToProps, mapDispatchToProps)(TodoList)