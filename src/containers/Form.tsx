import Form from '@components/Form'
import { actions } from '@modules/TodoList'
import { connect } from 'react-redux'
import IFormProps from '../types/Form';
import ITodoEntityProps from '../types/Todo';

const mapStateToProps = (state: IFormProps) => {
  return { }
}

const mapDispatchToProps = (dispatch: any) => ({
  addTodo: (name: ITodoEntityProps['name']) => {
    dispatch(actions.addTodo({name}))
  }
})

export default connect(mapStateToProps, mapDispatchToProps)(Form)