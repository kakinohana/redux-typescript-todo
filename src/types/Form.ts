import ITodoEntityProps from './Todo'

export default interface IFormProps {
  addTodo: (name: ITodoEntityProps['name']) => void
}

export interface IFormState {
  value: string
  inputWidth: number
  buttonWidth: number
  placeholderColor: string
}