import ITodoEntityProps from './Todo'

export default interface ITodoListEntityProps {
  todos: ITodoEntityProps[]
}

export interface ITodoListEntityComponentsProps {
  todos: ITodoEntityProps[]
  toggleTodo: (todo: ITodoEntityProps) => void
}