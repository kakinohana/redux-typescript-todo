export default interface ITodoEntityProps {
  id: number
  name: string
  completed: boolean
}