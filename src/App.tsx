import Header from '@components/Header'
import Form from '@containers/Form'
import TodoList from '@containers/TodoList'
import * as React from 'react';
import { injectGlobal } from 'styled-components'

/**
 * App
 */
class App extends React.Component {
  /**
   * render
   */
  public render() {
    return (
      <div>
        <Header text={'TODO'}/>
        <TodoList />
        <Form />
      </div>
    );
  }
}

export default App

// tslint:disable-next-line:no-unused-expression
injectGlobal`
  html {
    font-family: "";
    line-height: 1.5;
    font-size: 15px;
    body {
      margin: 30px;
      padding: 0;
    }
  }
`